package net.mineclick.game.minigames;

import lombok.Getter;

@Getter
public abstract class MiniGameService {
    protected GameState currentState;
}
