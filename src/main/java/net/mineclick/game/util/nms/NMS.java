package net.mineclick.game.util.nms;

import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import net.mineclick.game.Game;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.npc.VillagerData;

import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;

public class NMS {
    public final static WrappedDataWatcher.Serializer BOOLEAN_TYPE = WrappedDataWatcher.Registry.get(Boolean.class);
    public final static WrappedDataWatcher.Serializer VILLAGER_DATA_TYPE = WrappedDataWatcher.Registry.get(VillagerData.class);
    public final static WrappedDataWatcher.Serializer CHAT_COMPONENT_TYPE_OPT = WrappedDataWatcher.Registry.getChatComponentSerializer(true);
    public final static WrappedDataWatcher.Serializer CHAT_COMPONENT_TYPE = WrappedDataWatcher.Registry.getChatComponentSerializer();
    public final static WrappedDataWatcher.Serializer BYTE_TYPE = WrappedDataWatcher.Registry.get(Byte.class);
    public final static WrappedDataWatcher.Serializer INT_TYPE = WrappedDataWatcher.Registry.get(Integer.class);
    public final static WrappedDataWatcher.Serializer BLOCK_POS = WrappedDataWatcher.Registry.getBlockPositionSerializer(false);
    public final static WrappedDataWatcher.Serializer ITEM = WrappedDataWatcher.Registry.getItemStackSerializer(false);

    public static int createId() {
        try {
            // TODO nms mappings bullshit
            Field field = Entity.class.getDeclaredField("d"); // this is fine... https://wagyourtail.xyz/Projects/MinecraftMappingViewer/App?version=1.20.1&mapping=MOJMAP&search=net/minecraft/world/entity/Entity
            field.setAccessible(true);

            return ((AtomicInteger) field.get(null)).incrementAndGet();
        } catch (Exception e) {
            Game.i().getLogger().log(Level.SEVERE, "Unable to get NPC id", e);
            return Game.getRandom().nextInt();
        }
    }
}
