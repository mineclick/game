package net.mineclick.game.model;

import org.bukkit.Location;

public record EasterEgg(Location location, String id) {
}
